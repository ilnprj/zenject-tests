﻿public interface ICharacter
{
    float SpeedCharacter { get; set; }
    float Health { get; set; }
    float Stamina { get; set; }
}
