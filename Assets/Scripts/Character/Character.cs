﻿using UnityEngine;
using Zenject;

public class Character : MonoBehaviour, ICharacter
{
    [Inject]
    public DataCharacter characterParams;

    public float Health
    {
        get; set;
    }

    public float Stamina {
        get; set;
    }
    public float SpeedCharacter
    {
        get; set;
    }

    private void Start()
    {
        BindData();
    }

    private void BindData()
    {   
        Health = characterParams.Health;
        Stamina = characterParams.Stamina;
        SpeedCharacter = characterParams.SpeedMovement;
    }
}
