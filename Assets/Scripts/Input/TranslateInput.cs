﻿using UnityEngine;
using Zenject;

public class TranslateInput : MonoBehaviour, ICharacterInput
{
    private ICharacter _characterItentity;
    
    [Inject]
    private void CharacterBind(ICharacter characterInput)
    {
        _characterItentity = characterInput;
    }

    public float MoveSpeed => _characterItentity.SpeedCharacter;

    private void Update()
    {
        Move();
    }

    public void Move()
    {
        var moveHorizontal = Input.GetAxis("Horizontal")*MoveSpeed;
        var moveVertical = Input.GetAxis("Vertical")*MoveSpeed;
        var movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
        transform.Translate(movement);
    }
}
