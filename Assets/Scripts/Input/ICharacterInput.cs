﻿
public interface ICharacterInput 
{
    float MoveSpeed { get; }
    void Move();
}
