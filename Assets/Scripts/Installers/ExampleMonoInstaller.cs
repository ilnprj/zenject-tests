using Zenject;

/// <summary>
/// Образец Mono Installer. Объект производит установку связей
/// </summary>
public class ExampleMonoInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        ///Связь по принципу - объект уже есть на сцене, надо его просто привязать.
        Container.Bind<ICharacter>().FromInstance(FindObjectOfType<Character>());
        Container.Bind<TranslateInput>().FromInstance(FindObjectOfType<TranslateInput>());
        
        ///Связь по принципу - надо выполнить реализацию интерфейса
        //Container.Bind<ICharacter>().To<Character>().AsSingle();
        //Container.Bind<InputTest>().AsSingle().NonLazy();
    }
}