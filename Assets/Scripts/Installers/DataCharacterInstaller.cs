﻿using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "DataCharacterInstaller", menuName = "Installers/DataCharacterInstaller", order = 0)]
public class DataCharacterInstaller:  ScriptableObjectInstaller<DataCharacterInstaller>
{
    public DataCharacter dataCharacter;

    public override void InstallBindings()
    {
        Container.BindInterfacesAndSelfTo<DataCharacter>().FromInstance(dataCharacter).AsSingle();
    }
}
