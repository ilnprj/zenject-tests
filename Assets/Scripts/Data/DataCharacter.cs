﻿using UnityEngine;

[CreateAssetMenu (fileName = "DataCharacter", menuName = "Data/DataCharacter", order = 0)]
public class DataCharacter : ScriptableObject
{
    [SerializeField]
    private float health;

    public float Health => health;


    [SerializeField]
    private float speedMovement;

    public float SpeedMovement => speedMovement;

    [SerializeField]
    private float stamina;

    public float Stamina => stamina;
}
